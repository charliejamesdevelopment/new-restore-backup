function add_students() {
  var numItems = $('#accounts_table input:checked').length;
  if(numItems == 1) {
    $('#accounts_table input:checked').each(function(i,obj) {
        var apid = $(obj).val()
        getData(getUrl() + "/api/get_user_data/" + apid, function(user) {
          if(user.response != 1) {
              getData(getUrl() + "/admin_api/get_all_students", function (data) {
                if(data.response != 1) {
                  var students_obj = []
                  for(var i = 0; i < data.length; i++) {
                    var student = data[i];
                    students_obj.push({id : student._id, name: student.name, year: "Year 10"})
                  }
                  var value_students = []
                  var div = "<div class='form-group'>";
                  var divclose = "</div>";
                  var number_processed = 0;
                  var select_students = div + "<label for='students' style='text-align:left;'>Assign Students</label><input id='students' value='"+user.students.join()+"' placeholder='Enter students...'>" + divclose;
                  swal({
                    title: 'Editing Children for ' + user.name,
                    text: 'Simply click on the box below and search for the student, and remove students by clicking the "X" next to their name.',
                    type: 'info',
                    width: '80%',
                    html:
                      select_students,
                    preConfirm: function () {
                      return new Promise(function (resolve) {
                        resolve([
                          $('#students').val()
                        ])
                      })
                    },
                    onOpen: function () {
                      $('#students').selectize({
                        plugins: ['remove_button'],
                        persist: false,
                        maxItems: null,
                        valueField: 'id',
                        labelField: 'name',
                        searchField: ['name', 'year'],
                        options: students_obj,
                        render: {
                         item: function(item, escape) {
                           return '<div>' +
                               (item.name ? '<span class="name">' + escape(item.name) + ' (</span>' : '') +
                               (item.year ? '<span>' + escape(item.year) + ')</span>' : '') +
                           '</div>';
                         },
                         option: function(item, escape) {
                             var label = item.year;
                             var caption = item.name;
                             return '<div>' +
                                 '<div class="label" style="padding:0px;margin:0px">' + escape(label) + '</div>' +
                                 (caption ? '<div class="caption">' + escape(caption) + '</div>' : '') +
                             '</div>';
                         }
                       },
                      });
                    }
                  }).then(function (result) {
                    var students_obj = {
                      _id: apid,
                      students: result[0]
                    }
                    postData(getUrl() + "/admin_api/student_editor", students_obj, function(callback){
                      if(callback.response != 1) {
                        swal({
                          title: 'Success!',
                          type: 'success',
                          html:
                            "Account has been successfully edited!",
                          confirmButtonText: 'Ok',
                          showCancelButton: false,
                          allowOutsideClick: false
                        }).catch(swal.noop);
                      } else {
                        swal({
                          title: 'Oops!',
                          type: 'error',
                          html:
                            callback.message,
                          confirmButtonText: 'Ok',
                          showCancelButton: false,
                          allowOutsideClick: false
                        }).catch(swal.noop);
                      }
                    });
                  }).catch(swal.noop)
                } else {
                  swal({
                    title: 'Oops!',
                    type: 'error',
                    html:
                      data.message,
                    confirmButtonText: 'Ok',
                    showCancelButton: false,
                    allowOutsideClick: false
                  }).catch(swal.noop);
                }

              })
          } else {
            swal({
              title: 'Oops!',
              type: 'error',
              html:
                user.message,
              confirmButtonText: 'Ok',
              showCancelButton: false,
              allowOutsideClick: false
            }).catch(swal.noop);
          }
        });
    });
  } else {
    swal({
      title: 'Oops!',
      type: 'error',
      html:
        "Please select an account.",
      confirmButtonText: 'Ok',
      showCancelButton: false,
      allowOutsideClick: false
    }).catch(swal.noop);
  }
}
function view() {
  var numItems = $('#accounts_table input:checked').length;
  if(numItems == 1) {
    $('#accounts_table input:checked').each(function(i,obj) {
        var apid = $(obj).val()
        var newtab = window.open( '', '_blank' );
        newtab.location = getUrl() + '/main/view_profile/' + apid;
    });
  } else {
    swal({
      title: 'Oops!',
      type: 'error',
      html:
        "Please select an account.",
      confirmButtonText: 'Ok',
      showCancelButton: false,
      allowOutsideClick: false
    }).catch(swal.noop);
  }
  /*$('#dashboard_table input:checked').each(function(i,obj) {
      var apid = $(obj).val()
  });*/
}
function edit() {
  var numItems = $('#accounts_table input:checked').length;
  if(numItems == 1) {
    var apid = $('#accounts_table input:checked').first().val()
    getData(getUrl() + "/api/get_user_data/" + apid, function(response) {
      if(response.response != 1) {
        var account = response;
        getData(getUrl() + "/api/get_groups/", function(groups) {
          if(groups.response != 1) {
            var div = "<div class='form-group'>";
            var divclose = "</div>";
            var name = div + "<label for='name' style='text-align:left;'>Name</label><input id='name' value='"+response.name+"' class='form-control input-lg' placeholder='Enter full name...'>" + divclose;
            var email = div + "<label for='email' style='text-align:left;'>Email</label><input id='email' value='"+response.email+"' class='form-control input-lg' placeholder='Enter email address...'>" + divclose;
            var select_group = div + "<label for='group' style='text-align:left;'>Group</label><select id='group' class='form-control sw2 input-lg'>" + divclose;
            for(var i = 0; i < groups.length; i++) {
              if(response.group == groups[i].group_id) {
                select_group = select_group + "<option selected value='" + groups[i].group_id + "'>"+ groups[i].group_name +"</option>"
              } else {
                select_group = select_group + "<option value='" + groups[i].group_id + "'>"+ groups[i].group_name +"</option>"
              }
              if(i+1 == groups.length) {
                select_group = select_group + "</select></div>";
              }
            }
            var bio = div + "<label for='bio' style='text-align:left;'>Bio</label><textarea id='bio' class='form-control input-lg' placeholder='Enter bio...'>"+ response.bio + "</textarea>" + divclose;
            var password = div + "<label for='password' style='text-align:left;'>Password<br/><small class='note'>Only enter if you want to change existing password.</small></label><input id='password' class='form-control input-lg' placeholder='Enter new password...'>" + divclose;
            swal({
              title: 'User: ' + response.name,
              type: 'info',
              width: '70%',
              html:
                name +
                email +
                select_group +
                bio +
                password,
              preConfirm: function () {
                return new Promise(function (resolve, reject) {
                  if($('#name').val() != ''
                  && $('#email').val() != ''
                  && $('#group').val() != '') {
                    if($('#password').val() == '') {
                      resolve([
                        $('#name').val(),
                        $('#email').val(),
                        $('#bio').val(),
                        $('#group').val(),
                        $('#students').val()
                      ])
                    } else {
                      resolve([
                        $('#name').val(),
                        $('#email').val(),
                        $('#bio').val(),
                        $('#group').val(),
                        $('#password').val()
                      ])
                    }
                  } else {
                    reject('Please fill in the fields: name, email and group!')
                  }
                })
              },
            }).then(function (result) {
              if(result.length == 5) {
                var data = {
                  _id : response._id,
                  name : result[0],
                  email : result[1],
                  bio : result[2],
                  group : result[3],
                  password_changed: true,
                  password: result[4]
                }
              } else {
                var data = {
                  _id : response._id,
                  name : result[0],
                  email : result[1],
                  bio : result[2],
                  group : result[3],
                  password_changed: false
                }
              }
              postData(getUrl() + '/admin_api/edit_account', data, function(res) {
                if(res.response != 1) {
                  swal({
                    title: 'Success!',
                    type: 'success',
                    html:
                      "You have successfully edited " + response.name + "'s account!",
                    confirmButtonText: 'OK!',
                    showCancelButton: false,
                    allowOutsideClick: false
                  }).then(function() {
                    location.reload();
                  }).catch(swal.noop);
                } else {
                  swal({
                    title: 'Oops!',
                    type: 'error',
                    html:
                      res.message,
                    confirmButtonText: 'Ok',
                    showCancelButton: false,
                    allowOutsideClick: false
                  }).catch(swal.noop);
                }
              });
            }).catch(swal.noop);
          } else {
            swal({
              title: 'Oops!',
              type: 'error',
              html:
                groups.message,
              confirmButtonText: 'Ok',
              showCancelButton: false,
              allowOutsideClick: false
            }).catch(swal.noop);
          }
        });
      } else {
        swal({
          title: 'Oops!',
          type: 'error',
          html:
            response.message,
          confirmButtonText: 'Ok',
          showCancelButton: false,
          allowOutsideClick: false
        }).catch(swal.noop);
      }
    })
  } else {
    swal({
      title: 'Oops!',
      type: 'error',
      html:
        "Please select an account.",
      confirmButtonText: 'Ok',
      showCancelButton: false,
      allowOutsideClick: false
    }).catch(swal.noop);
  }
}
function delete_acc() {
  var numItems = $('#accounts_table input:checked').length;
  if(numItems >= 1) {
    swal({
      title: 'Confirm?',
      type: 'info',
      html:
        "Are you sure you want to delete " + numItems + " account(s)?",
      confirmButtonText: 'Yes',
      showCancelButton: true,
      allowOutsideClick: false
    }).then(function () {
      $('#accounts_table input:checked').each(function(i,obj) {
          var apid = $(obj).val()
          getData(getUrl() + "/admin_api/delete_account/" + apid, function(response) {
            if(response.response == 1) {
              swal({
                title: 'Error',
                text: response.message,
                type: 'error',
                confirmButtonText: 'Oops?',
                showCancelButton: false,
                allowOutsideClick: false
              }).catch(swal.noop);
            } else {
              if((i+1) == numItems) {
                location.reload();
              }
            }
          });
      });
    }).catch(swal.noop);
  } else {
    swal({
      title: 'Oops!',
      type: 'error',
      html:
        "Please select an account.",
      confirmButtonText: 'Ok',
      showCancelButton: false,
      allowOutsideClick: false
    }).catch(swal.noop);
  }
}
$(document).ready(function () {
  $('.account_group').each(function(i, obj) {
    var id = $(obj).text();
    getData(getUrl() + '/api/get_group_by_uuid/' + id, function(callback) {
      if(callback.response != 1) {
        $(obj).css('text-align', 'center');
        $(obj).css('color', '#fff');
        $(obj).css('padding', '10px');
        $(obj).css('background', callback.color);
        $(obj).text(callback.group_name);
      }
    });
  });

  $('.checkAll').on('click', function () {
    $(this).closest('table').find('tbody :checkbox')
      .prop('checked', this.checked)
      .closest('tr').toggleClass('selected', this.checked);
  });

  $('tbody :checkbox').on('click', function () {
    $(this).closest('tr').toggleClass('selected', this.checked);
    $(this).closest('table').find('.checkAll').prop('checked', ($(this).closest('table').find('tbody :checkbox:checked').length == $(this).closest('table').find('tbody :checkbox').length)); //Tira / coloca a seleção no .checkAll
  });
});

$('.avatar-student-year').each(function(i,obj) {
  uuid = children[i]._id;
  getData(getUrl() + "/api/get_year_data/"+uuid, function(result) {
    $(obj).text(result.name);
  });
});
