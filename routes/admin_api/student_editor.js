var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');

router.post('/student_editor',function(req,res){
    if(req.body._id && req.body.students) {
      utils.studentEditAccount(req.body, function(callback) {
        if(callback == 1) {
          res.send({"response" : 1, "message" : "Invalid id!"});
        } else {
          res.send({"response" : 0});
        }
      });
    } else {
      res.send({"response" : 1, "message" : "Please fill all fields!"});
    }
});

module.exports = router;
