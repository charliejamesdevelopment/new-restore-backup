var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');

router.get('/delete_appointment/:id',function(req,res){
    utils.getAppointmentData(req.params.id, function(appointment_data) {
      if(appointment_data == 1) {
        res.send({"response" : 1, "message" : "Invalid appointment!"});
      } else {
        if(appointment_data.status != 4) {
          utils.getGroupByUUID(req.session.uuid, function(group) {
            if(group == 1) {
              res.send({"response" : 1, "message" : "Invalid uuid!"});
            } else {
              console.log('Received appointment request by ' + req.session.uuid + ' (group: '+JSON.stringify(group)+') for Appointment#'+appointment_data._id);
              if(group.admin == true || appointment_data.parent_id == req.session.uuid || appointment_data.teacher_id == req.session.uuid) {
                utils.deleteAppointment(req.params.id, function(call) {
                  if(call == 1) {
                    onsole.log('Something went wrong: ' + req.params.id);
                    res.send({"response" : 1, "message" : "Invalid id!"});
                  } else {
                    res.send({"response" : 0});
                  }
                });
              } else {
                res.send({"response" : 1, "message" : "Buddy, you can't cancel this appointment!"});
              }
            }
          });
        } else {
          res.send({"response" : 1, "message" : "Emm, why are you trying to cancel an appointment thats already been cancelled?"});
        }
      }
    });
});

module.exports = router;
