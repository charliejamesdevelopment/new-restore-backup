var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
router.post('/edit_account',function(req,res){
  if(req.session.uuid) {
    name = req.body.name;
    bio = req.body.bio;
    utils.updateAccount(req.session.uuid, name, bio, function(call) {
      if(call == 1) {
        res.send({"response" : 1, "message" : "Invalid user!"});
      } else {
        res.send({"response" : 0});
      }
    });
  }
});

module.exports = router;
