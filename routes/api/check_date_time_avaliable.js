var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var bcrypt = require('bcrypt-nodejs');
var moment = require('moment');
router.get('/check_date_time_avaliable/:date/:time/:teacher_id',function(req,res){
    utils.checkAppointmentAvaliability({"date" : req.params.date, "time" : req.params.time, "teacher_id" : req.params.teacher_id}, function(result) {
      if(result.response == 1) {
        var date = moment(req.params.date).format("MMM Do YYYY");
        res.send({"response" : "1","message" : date + " at " + req.params.time + " has already been taken!"})
      } else {
        res.send({"response" : "0"});
      }
    });
});

module.exports = router;
