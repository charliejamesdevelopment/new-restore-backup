var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');


router.get('/',function(req,res){
  utils.isLoggedIn(req,function(call) {
    if(call == false) {
      res.redirect('/');
    } else {
      req.session = null;
      res.redirect('/');
    }
  });
});

module.exports = router;
