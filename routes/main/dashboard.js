var express = require('express');
var router = express.Router();
var utils = require('../utils/utils');
var config = require('../../config.js');
var appointment_utils = require('../utils/appointments.js');
var year_utils = require('../utils/year');
router.get('/:show_hidden',function(req,res){
  utils.isLoggedIn(req,function(call) {
    if(call == false) {
      res.redirect('/');
    } else {
      utils.getUserData(req.session.uuid, function(obj) {
        if(obj == 1) {
          console.log("Couldn't find data for " + req.session.uuid);
          req.flash("message", "Couldn't find data")
          req.flash("message_type", "error")
          res.redirect('/error');
        } else {
          res.status(200)
          utils.getGroupByUUID(req.session.uuid, function(g) {
            utils.getChildrenByUUID(req.session.uuid, function(data) {
              if(req.params.show_hidden && req.params.show_hidden == "true") {
                utils.getAppointmentsByUUIDwithHidden(req.session.uuid, function(appointments) {
                  if(data == 1 || appointments == 1) {
                    req.flash('error', 'Something went wrong...')
                    res.redirect("/error");
                  } else {
                    res.render('main/dashboard', {
                      year_utils: year_utils,
                      getStatus: appointment_utils.getStatus,
                      getAppointmentStatuses: appointment_utils.getAppointmentStatuses,
                      times: config.appointment.times,
                      types: config.appointment.types,
                      admin: g.admin,
                      user: obj,
                      group: g.name,
                      data: data,
                      appointments: appointments,
                      active: "Appointments",
                      button_text: "Hide Cancelled Appointments",
                      button_url: "/main/dashboard/false"
                    });
                  }
                });
              } else if(req.params.show_hidden == "false"){
                utils.getAppointmentsByUUIDwoHidden(req.session.uuid, function(appointments) {
                  if(data == 1 || appointments == 1) {
                    req.flash('error', 'Something went wrong...')
                    res.redirect("/error");
                  } else {
                    res.render('main/dashboard', {
                      year_utils: year_utils,
                      getStatus: appointment_utils.getStatus,
                      getAppointmentStatuses: appointment_utils.getAppointmentStatuses,
                      times: config.appointment.times,
                      types: config.appointment.types,
                      admin: g.admin,
                      user: obj,
                      group: g.name,
                      data: data,
                      appointments: appointments,
                      active: "Appointments",
                      button_text: "Show Cancelled Appointments",
                      button_url: "/main/dashboard/true"
                    });
                  }
                });
              } else {
                req.flash('error', 'Something went wrong...')
                res.redirect("/error");
              }
            });
          });
        }
      });
    }
  });
});

module.exports = router;
