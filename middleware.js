var jwt = require("jsonwebtoken");
var keys = require("./routes/database/keys");

admin_authentication = function(req, res, next) {
  admin_authentication_func(req,res,function(result) {
    if(result == false){
      return res.status(403).send({ success: false, message: 'Failed to authenticate token.'});
    } else {
      req.decoded = result;
      next();
    }
  });
}

admin_authentication_func = function(req, res, next) {

  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  // decode token
  if (token) {
    jwt.verify(token.toString(), keys.SECRET_ADMIN_KEY.toString(), function(err, decoded) {
      if(err) {
        next(false);
      } else {
        next(decoded);
      }

    });
  } else {
    next(false)
  }
}

authentication = function(req, res, next) {
  admin_authentication_func(req,res,function(result) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];
    if(result == false){
      if (token) {
        jwt.verify(token.toString(), keys.SECRET_PARENT_KEY.toString(), function(err, decoded) {
          if(err) {
            return res.status(403).send({ success: false, message: 'Failed to authenticate token.'});
          } else {
            req.decoded = decoded;
            next();
          }

        });
      } else {
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
      }
    } else {
      req.decoded = result;
      next();
    }
  });
}

module.exports = {
  authentication: authentication,
  admin_authentication:admin_authentication
}
